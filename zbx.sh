# FPM
yum install grep gawk lsof jq fcgi unzip bc
curl -L $(curl -s https://api.github.com/repos/rvalitov/zabbix-php-fpm/releases/latest | grep 'zipball_' | cut -d\" -f4) --output /tmp/zabbix-php-fpm.zip
unzip -j /tmp/zabbix-php-fpm.zip "*/zabbix/*" "*/ispconfig/*" -d /tmp/zabbix-php-fpm
cp /tmp/zabbix-php-fpm/userparameter_php_fpm.conf $(find /etc/zabbix/ -name zabbix_agentd*.d -type d | head -n1)
sudo cp /tmp/zabbix-php-fpm/zabbix_php_fpm_discovery.sh /etc/zabbix/
sudo cp /tmp/zabbix-php-fpm/zabbix_php_fpm_status.sh /etc/zabbix/
chmod +x /etc/zabbix/zabbix_php_fpm_discovery.sh
chmod +x /etc/zabbix/zabbix_php_fpm_status.sh
echo "zabbix ALL = NOPASSWD: /etc/zabbix/zabbix_php_fpm_discovery.sh,/etc/zabbix/zabbix_php_fpm_status.sh" >> /etc/sudoers

# NGINX