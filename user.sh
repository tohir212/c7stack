# USER {sysdevops, support, app}
echo -e '\n[Create New User, set passwd] // sysdevops'
echo -e 'If this field is left blank, the default account listed will be updated.'
read -p "Username[$SUDO_USER]: " -r newuser
if [ -z "$newuser" ]; then
  newuser=$SUDO_USER
fi
egrep "^$newuser" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
  echo "$newuser exists, skipping ahead..."
else
  unset newpass;
  echo -n 'Password: '
  while IFS= read -r -s -n 1 newchar; do
    if [[ -z $newchar ]]; then
       echo
       break
    else
       echo -n '*'
       newpass+=$newchar
    fi
  done
  useradd $newuser -s /bin/bash -m
  echo "$newuser:$newpass" | chpasswd
fi

## access
usermod –aG wheel sysdevops
echo "sysdevops ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# USER {sysdevops, support, app}
echo -e '\n[Create New User, set passwd] // support'
echo -e 'If this field is left blank, the default account listed will be updated.'
read -p "Username[$SUDO_USER]: " -r newuser
if [ -z "$newuser" ]; then
  newuser=$SUDO_USER
fi
egrep "^$newuser" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
  echo "$newuser exists, skipping ahead..."
else
  unset newpass;
  echo -n 'Password: '
  while IFS= read -r -s -n 1 newchar; do
    if [[ -z $newchar ]]; then
       echo
       break
    else
       echo -n '*'
       newpass+=$newchar
    fi
  done
  useradd $newuser -s /bin/bash -m
  echo "$newuser:$newpass" | chpasswd
fi



# USER {sysdevops, support, app}
echo -e '\n[Create New User, set passwd] // app'
echo -e 'If this field is left blank, the default account listed will be updated.'
read -p "Username[$SUDO_USER]: " -r newuser
if [ -z "$newuser" ]; then
  newuser=$SUDO_USER
fi
egrep "^$newuser" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
  echo "$newuser exists, skipping ahead..."
else
  unset newpass;
  echo -n 'Password: '
  while IFS= read -r -s -n 1 newchar; do
    if [[ -z $newchar ]]; then
       echo
       break
    else
       echo -n '*'
       newpass+=$newchar
    fi
  done
  useradd $newuser -s /bin/bash -m
  echo "$newuser:$newpass" | chpasswd
fi