#Volume
## Add EBS Volume
file -s /dev/sdb
##mkfs -t ext4 /dev/sdb
mkfs -t xfs /dev/sdb
mkdir /data
mount /dev/sdb /data
## Automount EBS Volume on Reboot
cp /etc/fstab /etc/fstab.bak
##echo "/dev/sdb       /data   ext4    defaults,nofail        0       0" >> /etc/fstab
echo "/dev/sdb       /data   xfs    defaults,nofail        0       0" >> /etc/fstab
