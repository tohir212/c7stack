#!/bin/bash
#
# [Quick C7 Stack]
#
# GitLab:   https://gitlab.com/devops212/lempqc
# Modief by   wiros4bleng
#
bold=$(tput bold)
normal=$(tput sgr0)
cat <<!
${bold}[quick-stack] Epuskesmas Installation${normal}
CentOS 7
!

# Inisialisasi
yum update
yum upgrade
yum install epel-release -y
yum install git nano zip unzip wget curl htop atop -y

# USER {sysdevops, support, app}
echo -e '\n[Create New User, set passwd] // sysdevops'
echo -e 'If this field is left blank, the default account listed will be updated.'
read -p "Username[$SUDO_USER]: " -r newuser
if [ -z "$newuser" ]; then
  newuser=$SUDO_USER
fi
egrep "^$newuser" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
  echo "$newuser exists, skipping ahead..."
else
  unset newpass;
  echo -n 'Password: '
  while IFS= read -r -s -n 1 newchar; do
    if [[ -z $newchar ]]; then
       echo
       break
    else
       echo -n '*'
       newpass+=$newchar
    fi
  done
  useradd $newuser -s /bin/bash -m
  echo "$newuser:$newpass" | chpasswd
fi

## access
usermod –aG wheel sysdevops
echo "sysdevops ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# USER {sysdevops, support, app}
echo -e '\n[Create New User, set passwd] // support'
echo -e 'If this field is left blank, the default account listed will be updated.'
read -p "Username[$SUDO_USER]: " -r newuser
if [ -z "$newuser" ]; then
  newuser=$SUDO_USER
fi
egrep "^$newuser" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
  echo "$newuser exists, skipping ahead..."
else
  unset newpass;
  echo -n 'Password: '
  while IFS= read -r -s -n 1 newchar; do
    if [[ -z $newchar ]]; then
       echo
       break
    else
       echo -n '*'
       newpass+=$newchar
    fi
  done
  useradd $newuser -s /bin/bash -m
  echo "$newuser:$newpass" | chpasswd
fi



# USER {sysdevops, support, app}
echo -e '\n[Create New User, set passwd] // app'
echo -e 'If this field is left blank, the default account listed will be updated.'
read -p "Username[$SUDO_USER]: " -r newuser
if [ -z "$newuser" ]; then
  newuser=$SUDO_USER
fi
egrep "^$newuser" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
  echo "$newuser exists, skipping ahead..."
else
  unset newpass;
  echo -n 'Password: '
  while IFS= read -r -s -n 1 newchar; do
    if [[ -z $newchar ]]; then
       echo
       break
    else
       echo -n '*'
       newpass+=$newchar
    fi
  done
  useradd $newuser -s /bin/bash -m
  echo "$newuser:$newpass" | chpasswd
fi

# LEMP 
## NGINX
echo -e '\n[NGINX]'
yum install nginx
systemctl enable nginx
systemctl start nginx
yes | cp /etc/nginx/fastcgi.conf /etc/nginx/fastcgi_params
yes | cp nginx/nginx.conf /etc/nginx/
mkdir /etc/nginx/sites-available/
mkdir /etc/nginx/sites-enabled/
cp nginx/localhost.conf /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/localhost.conf /etc/nginx/sites-enabled/
cp nginx/epus.conf /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/epus.conf /etc/nginx/sites-enabled/
mkdir /var/www/html/apps_ng
chown -R app.nginx /var/www/html/apps_ng
find /var/www/html/ -type d -exec chmod g+s {} \;
## access
usermod -aG nginx app
## access
usermod -aG nginx support

## Install multiple php5.6 dan php7.3
echo 'Install multiple php56 & php73'
yum install scl-utils -y
yum install epel-release -y
yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
echo -e '\n install php5.6?[y/n]'
yum install php56 php56-php php56-php-cli php56-php-mbstring php56-php-mcrypt php56-php-cli php56-php-mysql php56-php-pdo php56-php-xml php56-php-common php56-php-fpm php56-php-devel  php56-php-pear  php56-php-zip php56-php-gd -y 
echo -e '\n install php7.3?[y/n]'
yum install php73 php73-php php73-php-cli php73-php-mbstring php73-php-mcrypt php73-php-cli php73-php-mysql php73-php-pdo php73-php-xml php73-php-common php73-php-fpm php73-php-devel php73-php-pear  php73-php-zip php73-php-zmq php73-php-gd -y

sleep 5s
### Config pool
sed -i 's|listen = 127.0.0.1:9000|listen = /var/run/php-fpm.sock|g' /etc/opt/remi/php73/php-fpm.d/www.conf
sed -i 's|pm.max_children = 5|pm.max_children = 500|g' /etc/opt/remi/php73/php-fpm.d/www.conf
sed -i 's|pm.start_servers = 2|pm.start_servers = 20|g' /etc/opt/remi/php73/php-fpm.d/www.conf
sed -i 's|pm.min_spare_servers = 1|pm.min_spare_servers = 10|g' /etc/opt/remi/php73/php-fpm.d/www.conf
sed -i 's|pm.max_spare_servers = 3|pm.max_spare_servers = 64|g' /etc/opt/remi/php73/php-fpm.d/www.conf
sed -i 's|;pm.max_requests = 500|pm.max_requests = 500|g' /etc/opt/remi/php73/php-fpm.d/www.conf
sed -i 's|;pm.status_path = /status|pm.status_path = /php-fpm-status|g' /etc/opt/remi/php73/php-fpm.d/www.conf

# Volume
## Add EBS Volume
file -s /dev/sdb
##mkfs -t ext4 /dev/sdb
mkfs -t xfs /dev/sdb
mkdir /data
mount /dev/sdb /data
## Automount EBS Volume on Reboot
cp /etc/fstab /etc/fstab.bak
##echo "/dev/sdb       /data   ext4    defaults,nofail        0       0" >> /etc/fstab
echo "/dev/sdb       /data   xfs    defaults,nofail        0       0" >> /etc/fstab

# CertBot
yum install certbot-nginx -y
echo "15 3 * * * /usr/bin/certbot renew --quiet" >> /etc/crontab

# Zabbix-Agent
rpm -Uvh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-2.el7.noarch.rpm
yum install zabbix-agent -y

# NodeJS / pm2
yum install -y gcc-c++ make 
curl -sL https://rpm.nodesource.com/setup_14.x | sudo -E bash - 
yum install -y nodejs
yum install -y npm
npm i -g pm2

# Composer
/opt/remi/php73/root/bin/php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
HASH="$(wget -q -O - https://composer.github.io/installer.sig)"
/opt/remi/php73/root/bin/php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
/opt/remi/php73/root/bin/php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# Zabbix Monitoring NGinx, fpm, mysql

# Systemctl
systemctl enable nginx
systemctl start nginx
systemctl enable mariadb
systemctl restart mariadb
systemctl enable php73-php-fpm
systemctl start php73-php-fpm
systemctl disable php56-php-fpm
systemctl stop php56-php-fpm

echo
echo 'Completed..........!'
sh -c 'scl enable php73 bash'
exit 0
