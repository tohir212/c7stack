# MariaDB
echo 'Install mariaDB'
cp db/mariadb.repo /etc/yum.repos.d/
yum install MariaDB-server MariaDB-client -y
yes | cp db/server.conf /etc/my.cnf.d/server.cnf

# Move /var/lib/mysql
systemctl stop mariadb
sudo rsync -av /var/lib/mysql /data/
sudo mv /var/lib/mysql /var/lib/mysql.bak

systemctl enable mariadb
systemctl restart mariadb