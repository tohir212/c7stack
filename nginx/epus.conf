fastcgi_cache_path /etc/nginx/cache_kotakab levels=1:2 keys_zone=kotakab:100m inactive=60m;

server {
    listen 443 ssl;
    #listen 80;
    server_name kotakab.epuskesmas.id;
    root /var/www/html/apps_ng/kotakab/public;
    index index.php;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/ssl/certs/dhparam.pem;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_session_timeout 1d;
    #ssl_session_cache shared:SSL:50m;
    ssl_stapling off;
    resolver 127.0.0.1;
    ssl_stapling_verify on;

    add_header X-Frame-Options "ALLOW-FROM https://dashboard.epuskesmas.id";
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";

    # Content Security Policy

    add_header Public-Key-Pins 'pin-sha256="YLh1dUR9y6Kja30RrAn7JKnbQG/uEtLMkBgFF2Fuihg="; pin-sha256="sRHdihwgkaib1P1gxX8HFszlD+7/gTfNvuAybgLPNis="; max-age=5184000; includeSubDomains';
    add_header 'Referrer-Policy' 'no-referrer';
    add_header Expect-CT 'enforce; max-age=2764800';

    if ($request_method !~ ^(GET|PUT|POST|LOCK)$ ) {
        return 444;
    }

    if ($host !~ ^(kotakab.epuskesmas.id)$ ) {
        return 444;
    }

    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
        return 404;
    }

    location ~* ^.+\.(js|css|swf|xml|txt|ogg|ogv|svg|svgz|eot|otf|woff|woff2|mp4|ttf|rss|atom|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf|webp)$ {
        access_log off;
        log_not_found off;
        expires 45d;
        add_header Pragma public;
        add_header Cache-Control "public";
        add_header Access-Control-Allow-Origin *;
    }

    location ^~ /.well-known/ {
        allow all;
    }

    location / {
        # return 301 https://xyz.com/;
        try_files $uri $uri/ /index.php?$query_string;
    }

    if (!-d $request_filename) {
        rewrite ^/(.+)/$ /$1 permanent;
    }

#    location ~ [^/]\.php(/|$) {
location ~ (\.php|^/php-fpm-status)$ {
        fastcgi_split_path_info ^(.+?\.php)(/.*)$;
        if (!-f $document_root$fastcgi_script_name) {
            return 404;
        }
        # Mitigate https://httpoxy.org/ vulnerabilities
        fastcgi_param HTTP_PROXY "";
        fastcgi_pass unix:/var/run/php-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 12k;
        fastcgi_buffers 4 12k;

        fastcgi_cache_key "$scheme$request_method$host$request_uri";
        fastcgi_cache kotakab;
        fastcgi_cache_valid 200 60m;
    }
}